# The BuyBay client applications

React Hooks + Redux - User Registration, Login, Products, ProductDetail
UI React app for test backends requests.

## Setup

Instal node.js.

Install and start application.
```shell
npm install
npm start
```

## Getting Started

Visit http://localhost:8000/ and log in with admin credentials.